from random import randint

player = input("what is your name?: ")

# Guess 1

for guess_number in range(1, 6):
    guess_month = randint(1, 12)
    guess_year = randint(1924, 2004)

    print("Guess 1 :", player, "were you born in",
    guess_month, "/", guess_year, "?")

    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")
